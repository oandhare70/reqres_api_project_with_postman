# Introduction

 >- this project is like User Management System.
 >- about automating the test validation of different REST Api methods like Get, Post, Put, Patch, Delete etc. with the help of Postman tool.
 >- in the project we have used reqres API which is having date related to user & done automation testing on it. 

# Features & Functionality

 >- Used dynamic variables to fetch Random data for multiple test cases with the help of faker liberary.
 >- Data Driven Testing is done where in fetched different format of data like JSON  and CSV file format is used init.
 >- the project is able to retrive for the fixed number of iterations.
 >- Request chaining is done in the project so can exicute all the requests with one click.
 >- Used External Tool called **NEWMAN** to run collection from command line interface.
 >- Generated ClI Report, HTML Report & HTMLextra report with the help of newman.

